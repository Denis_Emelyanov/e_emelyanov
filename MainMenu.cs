﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	private int playButtonWidth = 50,
				playButtonHeight = 50;

	void OnGUI() {
		if (GUI.Button (new Rect ((Screen.width - playButtonWidth) / 2, Screen.height / 3 + 50, playButtonWidth, playButtonHeight), "Start"))
			Application.LoadLevel ("game");
	}
}
