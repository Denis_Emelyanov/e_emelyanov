﻿using UnityEngine;	// Уходи
using System.Collections;

public class GameOver : MonoBehaviour {
	
	private float textX = Screen.width / 5, textY = Screen.height / 4, textWidth = Screen.width * 3 / 5, textHeight = Screen.height / 2;
	private int mainMenuButtonWidth = 100, mainMenuButtonHeight = 50;
	
	void OnGUI() {	// Дверь
		GUI.Box(new Rect(textX, textY, textWidth, textHeight), "Game Over");
		GUI.Box(new Rect(textX, textY + 20, textWidth, textHeight - 20), "Score:\t\t\t\t" + Game.Instance.score.ToString() + "\n" +
		        														 "Highest score till game:\t\t" + Game.Instance.maxScore + "\n" + 
		        														 "Longest streak:\t\t\t" + Game.Instance.longestStreak);
		if (GUI.Button (new Rect (textX + (textWidth - mainMenuButtonWidth) / 2, textY + 100, mainMenuButtonWidth, mainMenuButtonHeight), "Main menu"))
			Application.LoadLevel ("main menu");
	}
}
// Закрой